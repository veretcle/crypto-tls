# Récupérer les informations d’un certificat X.509

Rendez-vous sur [cette URL](https://r.mateu.be/).

Votre navigateur récupère toutes les informations concernant le certificat X.509 de ce site, mais vous pouvez les récupérer très facilement aussi.

**Sous Firefox :**

Cliquez sur le cadenas à gauche de la barre d’adresse. Puis cliquez sur la flèche de droite dans la partie `Connexion` puis cliquez sur le bouton `Plus d’information` en bas de la fenêtre.

Vous avez alors une nouvelle fenêtre vous permettant d’`Afficher les détails` du certificat.

**Sous Chrome/Chromium :**

Cliquer sur le cadenas à gauche de la barre d’adresse, puis cliquez sur la partie `Certificat (Valide)`.

*Trouvez la date d’émission et d’expiration du certificat.
Trouvez l’algorithme de signature/hash du certificat.
Trouvez le nom de l’émetteur *Issuer* du certificat.
Trouvez également s’il y a des SANs associés à ce certificat.*

# Comprendre les erreurs liées au certificat

Faites pointer votre navigateur sur [cette adresse](https://mumble.nintendojo.fr/).

Essayez d’afficher les causes de l’erreur ou le certificat associé.

*Votre navigateur va vous afficher une erreur. Pourquoi ?*

Faites pointer votre navigateur vers [cette adresse](https://www.cacert.org/).

Essayez d’afficher les causes de l’erreur ou le certificat associé.

*Votre navigateur affiche également une erreur. Pourquoi ?*

