# Vérification des certificats et des *ciphers* avec SSLLabs


[SSLLabs](https://www.ssllabs.com/) est un service en ligne permettant de tester :
* la validité des certificats (comme le ferait votre navigateur)
* les différents *ciphers* utilisés par le site

**Votre navigateur choisira toujours le **meilleur** *cipher* pour se connecter à un site Web.** Néanmoins, la plupart des vulnérabilités TLS viennent des connexions les plus anciennes.

Cet outil va tester tous les protocoles (*SSLv2, SSLv3, TLS1.0, etc…*) mais également toutes les combinaisons de chiffrement possibles :
* plusieurs versions de AES
* plusieurs fonctions de hachage
* Diffie-Hellman

Testez les sites suivants et interprétez les résultats et notes donnés par [SSLLabs](https://www.ssllabs.com/ssltest/) :
* https://r.mateu.be/
* https://particuliers.societegenerale.fr/
* https://connect-racco.enedis.fr/
* https://www.credit-agricole.fr/
* https://www.cacert.org/
