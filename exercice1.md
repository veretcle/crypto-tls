# Sommes de contrôle

Créez un fichier (texte, Word, etc…) avec une simple phrase dedans. Enregistrez-le.

Ouvrez un terminal de commande et allez dans le dossier contenant ce fichier avec la commande `cd`. Calculez le hash du fichier pour différents algorithmes à l’aide des commandes suivantes :

```
CertUtil -hashfile <fichier> MD5
CertUtil -hashfile <fichier> SHA1
CertUtil -hashfile <fichier> SHA256
```

*Notez bien les sommes de contrôles.*

Réouvrez le fichier en question, faites **une seule modification** à l’intérieur et enregistrez-le. Recalculez les hash du fichier.

*Que remarquez-vous ?*

# Signature et chiffrement GPG

## Installation d’un logiciel GPG

Récupérez [GPG4Win](https://r.mateu.be/gpg4win-3.1.7.exe) et installez-le.

*Ne faites que l’installation de base, pas besoin de rajouter tous les modules :
* Désactiver au moins GPGOL (GPG pour Outlook)*

## Démarrage et création d’une paire de clés publique/privée

Démarrez `Kleopatra`, l’interface graphique de GPG.

Créez une nouvelle paire de clé publique/privée en suivant les instructions sur l’interface. Une fois la paire de clés crée, utiliser le bandeau pour exporter la clé publique dans un fichier.

Envoyez cette clé publique à l’une des autres personnes dans la salle.

## Import d’une clé publique existante

Quand vous aurez reçu la clé publique de quelqu’un d’autre, vous pourrez l’importer dans l’interface de `Kleopatra`. Au moment de l’importation, on vous demandera de vérifier l’identité du porteur de clé et son empreinte. Vérifiez toutes ces informations.

`Kleopatra` vous proposera ensuite de *certifier* la clé, c’est-à-dire de la signer avec votre propre clé privée. Effectuez cette opération.

## Signature d’un fichier

Reprenez le fichier du premier exercice. Signez-le à l’aide de `Kleopatra` :
* utilisez le menu dans le bandeau `Signer/chiffrer`
* sélectionnez le fichier
* décochez toutes les cases concernant le chiffrement
* cochez la case tout en bas `Chiffrer/signer chaque fichier séparément`

Envoyez le fichier et sa signature à la personne qui a récupéré votre clé publique précédemment.

## Vérification de la signature d’un fichier

Une fois que vous avez récupérer le fichier et sa signature, vérifiez-les avec `Kleopatra`.

*Note : le fichier et sa signature doivent être dans le même dossier pour faire la vérification*

*Pouvez-vous vérifier l’identité du signataire correctement ? Que se passe-t-il si vous modifiez le fichier et que vous revérifiez la signature ?*

## Chiffrement

Recommencez la même opération mais cette fois-ci en chiffrant le fichier au passage : vous pouvez sélectionner le destinataire du chiffrement dans le sous-menu `Chiffrer pour d’autres`.

Envoyez le fichier chiffré à son destinataire.

*Peut-il le déchiffrer ? Quelqu’un d’autre en a-t-il la capacité ?*

