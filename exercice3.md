# Créer une PKI

## Installer OpenSSL

Allez télécharger [l’archive OpenSSL](https://bintray.com/vszakats/generic/download_file?file_path=openssl-1.1.1b-win64-mingw.zip). Dézippez-la dans un répertoire.

Ouvrez un terminal de commande et placez dans le répertoire correspondant.

## Créer un certificat racine

Dans la fenêtre de terminal, tapez les commandes suivantes :

```
set OPENSSL_CONF=openssl.cnf
openssl.exe req -x509 -newkey rsa:4096 -nodes -sha256 -keyout macle.key -out moncert.crt -days 365
```

Répondez à l’ensemble des questions posées (mot de passe pour la clé privée, puis toutes les autres informations d’identité).

Une fois la procédure terminée, dans l’Explorateur de fichiers, vous devriez voir un fichier `moncert.crt` dans le dossier contenant `OpenSSL`. Ouvrez ce fichier et vérifier l’ensemble de votre certificat.

Vous pouvez également voir le détail avec la commande suivante :

```
openssl.exe x509 -in moncert.crt -noout -text
```

*Que constatez-vous concernant l’*Issuer* et le *Subject* ?*

## Créer un CSR

Toujours en ligne de commande et toujours dans le même dossier, vous allez faire une demande de certificat :

```
openssl.exe req -new -newkey rsa:2048 -nodes -sha256 -keyout server.key -out server.csr
```

*Note : à la question *A challenge password*, ne mettez surtout rien !*

Vous avez créé une clé privée mais au lieu d’un certificat, `OpenSSL` vous a généré un *Certificate Signing Request* que vous pouvez maintenant faire signer.

Comme précédemment, vous pouvez lire le contenu du *CSR* en utilisant la commande suivante :

```
openssl.exe req -in server.csr -noout -text
```

*Quelles informations manquent dans ce *CSR* ?*

## Signer un CSR et créer le certificat correspondant

Toujour en ligne de commande et dans le même dossier, la commande suivante va vous permettre de générer un certificat à partir d’une requête de signature :

```
openssl.exe x509 -req -sha256 -days 30 -in server.csr -CA moncert.crt -CAkey macle.key -CAcreateserial -out server.crt
```

*Vérifiez l’émetteur dans le fichier *server.crt* que vous venez de créer.*

Maintenant, envoyez votre *CSR* à une autre personne et demandez-lui de le signer. Récupérez le certificat résultant et comparez-le au vôtre.

*L’autorité de certificat *l’Émetteur* a certes changé entre les deux fichiers, mais que constatez-vous pour le reste ?*

