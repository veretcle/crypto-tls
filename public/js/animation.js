window.onload = function () {
	var anim_x509 = Snap("#x509");
	Snap.load("img/certificat_x509.svg", function(f){
		var sign = f.select('#sign');
		var identity = f.select('#identity');
		var virgin_certificate = f.select('virgin_certificate');
		var public_key = f.select('#public_key');
		var private_key = f.select('#private_key');

		identity.attr({ display: "none" });
		sign.attr({ display: "none" });

		public_key.click( function() {
			identity.attr({ display: "" });
		});

		identity.click( function() {
			sign.attr({ display: "" });
		})

		anim_x509.append(f);
	});
};

